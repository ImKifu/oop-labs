/**
 * @author Nguyen Kieu Hung
 * @version 1.0
 * @since 2018/10/02
 */
public class orange extends fruit {
    private String inDate;  
    private String original;

    public void setDate( String inDate){
        this.inDate = inDate;
    }
    public void getDate(){
        return inDate;
    }
    public void setOriginal( String original ){
        this.original = original;
    }
    public String getOriginal(){
        return original;
    }

}