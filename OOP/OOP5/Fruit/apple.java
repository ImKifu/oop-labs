
/**
 * @author Nguyen Kieu Hung
 * @version 1.0
 * @since 2018/10/02
 */
public class apple extends fruit {
    private String inDate;
    private String color = "Red";

    public void setDate( String inDate){
        this.inDate = inDate;
    }
    public String getDate(){
        return inDate;
    }
    public void setColor( String color ){
        this.color = color;
    }
    public String getColor(){
        return color;
    }
}