
/**
 * @author Nguyen Kieu Hung
 * @version 1.0
 * @since 2018/10/02
 */
public class fruit {
    private String nameOfFruit;
    private double price;

    public void setNameOfFruit( String name ){
        this.nameOfFruit = name;
    }

    public String getNameOfFruit() {
        return nameOfFruit;
    }
    public void setPrice( double price )
    {
        this.price = price; 
    }
    public double getPrice(){
        return price;
    }
}

