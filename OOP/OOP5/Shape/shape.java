/**
 * @author Nguyen Kieu Hung
 * @version 1.0
 * @since 2018/10/02
 */
public class static shape{
    private String color;
    private boolean filled = true;

    public shape(){}
    public shape( String color, boolean filled ){
        this.color = color;
        this.filled = filled;
    }
    public void setColor( String color ){
        this.color = color;
    }
    public String getColor() {
        return color;
    }
    public boolean isFilled(){
        return this.filled;
    }
    public void setFilled( boolean filled ){
        this.filled = filled ;
    }
    public String toString(){
        return color;
    }
}

