/**
 * @author Nguyen Kieu Hung
 * @version 1.0
 * @since 2018/10/02
 */
public class Circle extends shape{
    private double radius = 1.0;
    private final double pi = 3.14;

    public Circle(){}
    public Circle( double radius ){
        this.radius = radius;
    }
    public Circle( double radius, String color, boolean filled ){
        this.radius = radius;
        this.color = color;
        this.filled = filled;
    }
    public double getRadius(){
        return radius;
    }
    public void setRadius( double radius ){
        this.radius = radius;
    }
    //tinh dien tich
    public double getArea() {
        return this.pi * this.radius * this.radius;
    }
    //tinh chu vi
    public double getPerimeter() {
        return 2 * this.pi * this.radius;
    }
    public double getPi() {
        return pi;
    }
    public String toString(){
        return getRadius + " " + getArea + " " + getPerimeter + " " + getColor ;
    }

}