public class Student {
    private String name;
    private String id;
    private String group;
    private String email;




    //day la ham constructor
    public Student()
    {
        this( "Student","000" ,"K59CB","uet@vnu.edu.vn" ); //lay thong tin truyen vao tu ham constructor day du
    }

    public Student( String name, String id, String email)
    {
        this.name = name;
        this.id = id;
        this.group = "K59CB";
        this.email = email;
    }

    public Student( String name, String id, String group, String email )
    {
        this.name = name;
        this.id = id;
        this.group = group;
        this.email = email;
    }
    //ham copyConstructor
    Student( Student student )
    {
        this.name = student.getName();
        this.id = student.getId();
        this.group = student.getGroup();
        this.email = student.getEmail();
    }


    //cac nam setter getter
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getInfo(){
        String info;
        info = "Day la thong tin day du cua sinh vien " + getName()+ " :" + '\n'  + getId() + '\n'+ getGroup() + '\n'+ getEmail() ;
        System.out.println(info);
        return info;
    }
}
