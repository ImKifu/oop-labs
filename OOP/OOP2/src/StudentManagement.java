import java.util.ArrayList;

public class StudentManagement {

    static ArrayList<Student> studentsArray = new ArrayList<Student>(100);


    public static void main(String[] args) {
        Student student = new Student();
        student.setName("Hung");
        student.setId("17021269");
        student.setGroup("k62-clc1");
        student.setEmail("iamkifu@gmail.com");
        student.getName();
        //student.getInfo();

        Student demoCopyStudent = new Student(student);
        //demoCopyStudent.getInfo();

        Student studentDemo1 = new Student("Trang", "1234567890", "hungkieunguyen99@gmail.com");
        Student studentDemo2 = new Student("Shinobi", "1234", "somebodyisdaydreamer@gmail.com");
        //System.out.println(sameGroup(studentDemo1, studentDemo2));

        studentsArray.add(student);
        studentsArray.add(demoCopyStudent);
        studentsArray.add(studentDemo1);
        studentsArray.add(studentDemo2);
        studentsArray.add(student);
        removeStudent("1234567890");

        printStudentsByGroup("K59CB");


    }



    //ham so sanh
    public static boolean sameGroup(Student s1, Student s2) {
        if (s1.getGroup().equals(s2.getGroup())) {
            return true;
        }
        return false;
    }

    public static void printStudentsByGroup(String group){
        for( Student studentTemp : studentsArray )
        {
            if( studentTemp.getGroup().equals(group))
            {
                System.out.println(studentTemp.getInfo());
            }
        }
    }

    //ham remove
    public static void removeStudent(String id)
    {
        for( int i=0; i<studentsArray.size(); i++)
        {
            if( studentsArray.get(i).getId().equals(id))
            {
                studentsArray.remove(i);
            }
        }
    }


}
