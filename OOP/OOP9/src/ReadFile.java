/**
 * chuong trinh doc file va ghi file trong java
 * @author ImKifu
 * @version 2.0
 * @since 11/12/2018
 */
import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class ReadFile {

    public static String readContentFromFile(String path)
    {

        File text = new File( path );

        Scanner scanner = null;

        try {
            scanner = new Scanner(text);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        String s = " ";

        while (scanner.hasNextLine())
        {
            String line = scanner.nextLine();
            s += line + '\n';
            //System.out.println(line);
        }
        return s;
    }

    public static void writeContentToFile(String path, String content )
    {
        //BufferedWriter writer = null;
        try ( FileWriter fileWriter = new FileWriter( path, true );
                BufferedWriter bufferedWriter = new BufferedWriter(fileWriter) )
        {
            //String content = "Nội dung mình muốn viết vào file\n";
            bufferedWriter.write("\n" + content);
            // không cần đóng BufferedWriter (nó đã tự động đóng)
            // bw.close();
            //System.out.println("Da ghi chen vao cuoi file");
        } catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public static File findFileByName(String folderPath, String fileName)
    {
        File folder = new File(folderPath);
        ArrayList<File> listFile = new ArrayList<>();
        //File[] listFile = new File[100];
        //ArrayList<File> arrayList = new ArrayList<>();

        if( folder.isDirectory() )
        {
           // listFile = folder.listFiles();
            //listFile = Arrays.asList(folder.listFiles());
            Collections.addAll(listFile,folder.listFiles());
        }
        for( File file : listFile  )
        {
            if( file.getName().equals(fileName) )
            {
                return file;
            }
        }
        return null;
    }


}






