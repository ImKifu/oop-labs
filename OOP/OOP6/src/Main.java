/**
 * @author Nguyen Kieu Hung
 * @version 1.0
 * @since 2018/10/02
 */
public class Main {
    public static void main( String[] args )
    {
        Shape tamgiac = new Triangle(4,5,6,"Yellow",true,"triangle");
        Shape hinhchunhat = new Rectangle(4,5," Blue", true, "rectangle");
        Shape hinhlvuong = new Square(4,"Black",true,"square");
        Shape hinhtron = new Circle(3,"Navy",true,"circle");


        Shape tamgiac2 = new Triangle( 4, 5, 6, "Blue", true, "triangle");
        Shape tamgiac3 = new Triangle( 4, 5, 6, "Black", false, "triangle");



        Layer layer = new Layer();
        layer.add( tamgiac);
        layer.add( hinhchunhat);
        layer.add( hinhlvuong);
        layer.add(hinhtron);
        layer.add(tamgiac2);
        layer.add(tamgiac3);

        layer.deleteTriagle();

        //layer.deleteDuplicateShape();

        Diagram diagram = new Diagram();
        diagram.add(layer);
        diagram.deleteCircle();
        diagram.print();



    }





}
