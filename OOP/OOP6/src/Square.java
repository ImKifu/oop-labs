public class Square extends Rectangle {

    public Square() {
    }

    public Square(double side) {
        this.setWidth(side);
        this.setLength(side);
    }

    public Square(double side, String color, boolean filled, String type) {
        super(side, side, color, filled, type);
    }


    public double getSide() {
        return super.getWidth();
    }

    public void print()
    {
        System.out.println( getSide() + " " + super.getColor() + " " + super.getType() );
    }

    public boolean equals( Shape shape )
    {
      //  if (shape = null) {
      //      return false;
      //  }
        if (this.getClass() != shape.getClass()) {
            return false;
        }
        Square temp = (Square) shape;
        return (this.getToaDoX() == shape.getToaDoX() && this.getToaDoY() == shape.getToaDoY() && this.getSide() == ((Square) shape).getSide());
    }








}