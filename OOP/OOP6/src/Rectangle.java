/**
 * @author Nguyen Kieu Hung
 * @version 1.0
 * @since 2018/10/02
 */
public class Rectangle extends Shape{

    private double width_ = 2.0;
    private double length = 1.0;

    public Rectangle() {

    }

    public Rectangle(double width_, double length) {
        this.width_ = width_;
        this.length = length;
    }

    public Rectangle(double width_, double length, String color, boolean filled, String type) {
        super( color, filled, type );
        this.width_ = width_;
        this.length = length;

    }

    public double getArea() {
        return this.width_ * this.length;
    }

    public double getPerimeter() {
        return 2*(this.width_ + this.length);
    }
    //getter and setter
    public double getwidth_() {
        return width_;
    }

    public void setwidth_(double width_) {
        this.width_ = width_;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public void print()
    {
        System.out.println( getwidth_()+ " " + getLength() + " " + super.getColor() + " " + super.getType() );
    }

}
