import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * day la class sap xep cho cac kieu du lieu nguyen thuy
 * @author ImKifu
 * @version 2.0
 * @since 11/12/2018
 */
public class Template  {

    //ham nay sap xep theo thu tu tu be den lon theo thuat toan InterchangeSort
    public static <T extends Comparable<T>> void sort(ArrayList<T> list )
    {
        for( int i=0; i<list.size(); i++ )
        {
            for( int j=i+1; j<list.size(); j++ )
            {
                if( list.get(i).compareTo( list.get(j)) >0 )
                {
                    T temp = list.get(j);
                    list.set(j, list.get(i));
                    list.set(i, temp);
                }
            }
        }
    }


    // phuong thuc generic co ten la printArray
    //ham co chuc nang in ra cac gia tri cua array
    public static < E > void printArray( ArrayList<E> inputArray )
    {
        for ( E element : inputArray ){
            System.out.print( element + " ");
        }
    }

    //ham tim gia tri lon nhat cua m't mang du lieu nguyen thuy
    public static < K extends Comparable<K> > K findMaxOfArray(ArrayList<K> list )
    {
        K temp = list.get(0); // gan temp la phan tu dau tien cua list

        for( K element : list )
        {
            if(temp.compareTo(element)<0)
            {
                temp = element;
            }
        }
        System.out.println(temp);
        return temp;
    }




}

