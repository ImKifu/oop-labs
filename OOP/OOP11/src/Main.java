import java.util.ArrayList;

public class Main {
    public static void main( String[] args )
    {
        Template template = new Template();

        ArrayList<Integer> integerArrayList = new ArrayList<Integer>();
        integerArrayList.add(1);
        integerArrayList.add(8);
        integerArrayList.add(9);
        integerArrayList.add(0);

        template.sort(integerArrayList);
        template.printArray(integerArrayList);

        ArrayList<String> stringArrayList = new ArrayList<>();
        stringArrayList.add(" Hung");
        stringArrayList.add("ImKifu");
        stringArrayList.add("APG.Dreamlike");
        stringArrayList.add("Shinobi");
        template.sort(stringArrayList);
        template.printArray(stringArrayList);

        template.findMaxOfArray(integerArrayList);
        template.findMaxOfArray(stringArrayList);

    }
}
