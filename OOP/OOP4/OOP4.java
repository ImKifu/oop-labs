/**
 * chuong trinh tim max(a,b), min cua Array, tinh Bmi
 * @author ImKifu
 * @version 2.0
 * @since 11/12/2018
 */
public class OOP4 {

    /**
     * max la ham tim max cua 2 so nguyen
     * @return int tra ve so lon nhat trong 2 so a,b
     */
    public static int max(int a, int b)
    {
        return (a>b) ? a : b;
    }
    /**
     * minArr la ham tim min trong 1 mang
     * @param n la so phan tu cua mang
     * @param a la mang can tim min
     * @return int tra ve so nho nhat trong mang
     */
    public static int findMinArr(int n, int[] a)
    {
        int temp = a[0];
        for( int i=1; i<a.length; i++ )
            if ( a[i] < temp) 
            {
                temp = a[i];
            }
        return temp;
    }
    /**
     * checkBmi la ham tinh chi so Bmi roi nhan xet
     * @param weight la can nang
     * @param  height la chieu cao
     * @return String tra ve nhan xet voi tung chi so bmi
     */
    public static String checkBmi(double weight, double height)
    {
         double bmi = weight/(height*height);
         if (bmi < 18.5) return "Thieu can";
         if (bmi < 22.99) return "Binh thuong";
         if (bmi < 24.99) return "Thua can";
         return "Beo phi";
    }


}
