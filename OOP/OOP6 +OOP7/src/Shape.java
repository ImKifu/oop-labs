public class Shape extends Layer {

    /**
     * @author Nguyen Kieu Hung
     * @version 1.0
     * @since 2018/10/02
     */
    private String color;
    private boolean filled = true;
    private String type;

    public Shape() { }

    public Shape ( String type )
    {
        this.type = type;
    }

    public Shape(String color, boolean filled, String type) {
        this.color = color;
        this.filled = filled;
        this.type = type;
    }

    public void setType( String type ){
        this.type = type;
    }

    public String getType(){
        return type;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public boolean isFilled() {
        return this.filled;
    }

    public void setFilled(boolean filled) {
        this.filled = filled;
    }

    public String ŁtoString() {
        return color;
    }

}



