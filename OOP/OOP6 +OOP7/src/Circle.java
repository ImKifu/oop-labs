/**
 * @author Nguyen Kieu Hung
 * @version 1.0
 * @since 2018/10/02
 */
public class Circle extends Shape {
    private double radius = 1.0;
    private final double pi = 3.14;

    public Circle(){}

    public Circle(double radius) {
        this.radius = radius;
    }

    public Circle(double radius, String color, boolean filled, String type) {
        super(color, filled, type);
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    //tinh dien tich
    public double getArea() {
        return this.pi * this.radius * this.radius;
    }

    //tinh chu vi
    public double getPerimeter() {
        return 2 * this.pi * this.radius;
    }

    public double getPi() {
        return pi;
    }


    public String toString() {
        return getRadius() + " " + getArea() + " " + getPerimeter() + " " + getColor();
    }

    public void print()
    {
        System.out.println( getRadius() + " " + super.getColor() + " " + super.getType() );
    }
}