public class Triangle extends Shape {
    private double canhA, canhB, canhC;

    public Triangle() {
    }

    public Triangle(double a, double b, double c) {
        this.canhA = a;
        this.canhB = b;
        this.canhC = c;
    }

    public Triangle(double a, double b, double c, String color, boolean filled, String type) {
        super(color, filled, type);
        this.canhA = a;
        this.canhB = b;
        this.canhC = c;

    }

    public void setCanhA( double canhA)
    {
        this.canhA = canhA;
    }

    public void setCanhB(double canhB) {
        this.canhB = canhB;
    }

    public void setCanhC(double canhC) {
        this.canhC = canhC;
    }

    public double getCanhA() {
        return canhA;
    }

    public double getCanhB() {
        return canhB;
    }

    public double getCanhC() {
        return canhC;
    }

    public void print()
    {
        System.out.println( getCanhA()+" "+ getCanhB()+ " "+ getCanhC() + " " + super.getColor() + " " + super.getType() );
    }

    public boolean equals( Object shape )
    {
        //  if (shape = null) {
        //      return false;
        //  }
        if (this.getClass() != shape.getClass()) {
            return false;
        }
        Triangle temp = (Triangle) shape;
        return (this.getToaDoX() == temp.getToaDoX() && this.getToaDoY() == temp.getToaDoY() && this.getCanhA() == temp.getCanhA() && this.getCanhB() == temp.getCanhB() && this.getCanhC() == temp.getCanhC()  );
    }

    




}