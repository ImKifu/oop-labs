import java.util.LinkedList;

public class Layer extends Diagram {
    private LinkedList<Shape> listShapes = new LinkedList<>();
    private int toaDoX, toaDoY;
    private boolean visible;
    /**
     * khoi tao constructor rong
     */
    public Layer(){}

    public Layer(int toaDoX, int toaDoY) {
        this.toaDoX = toaDoX;
        this.toaDoY = toaDoY;
    }

    public void setToaDoX(int toaDoX) {
        this.toaDoX = toaDoX;
    }

    public void setToaDoY(int toaDoY) {
        this.toaDoY = toaDoY;
    }

    public int getToaDoX() {
        return toaDoX;
    }

    public int getToaDoY() {
        return toaDoY;
    }

    public void deleteTriagle(){
        for( int i=listShapes.size()-1; i>=0; i-- )
        {
            if( listShapes.get(i).getType().equals("triangle"))
            {
                listShapes.remove(i);

            }
        }
    }

    public void deleteCircle(){
        for( int i=0; i<listShapes.size(); i++ )
        {
            if( listShapes.get(i).getType().equals("circle"))
            {
                listShapes.remove(i);
            }
        }
    }

    public void add(Shape shape)
    {
        listShapes.add(shape);
    }

    public void print(){
        for( int i=0; i<listShapes.size(); i++ )
        {
            listShapes.get(i).print();
        }
    }

    public void setVisible(boolean visible)
    {
        this.visible = visible;
    }

    public boolean getVisible()
    {
        return visible;
    }

    public boolean isEqual( Shape shape1, Shape shape2 )
    {
        if( shape1.getType().equals(shape2.getType()) &&
            shape1.getToaDoX() == shape2.getToaDoX() &&
            shape1.getToaDoY() == shape2.getToaDoY() )
        {
            return true;
        }
        return false;
    }

    public void deleteDuplicateShape()
    {
        for( int i=0; i<listShapes.size(); i++ )
        {
            for( int j=i+1; j<listShapes.size(); j++ )
            {
                if( listShapes.get(i).equals(listShapes.get(j)) )
                {
                    listShapes.remove(listShapes.get(j)) ;
                }
            }
        }
    }

}
