import java.util.ArrayList;
import java.util.LinkedList;

public class Diagram {
    private double width, height;
    private LinkedList<Layer> listlayers = new LinkedList<>();

    public void setWidth(double width) {
        this.width = width;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getWidth() {
        return width;
    }

    public double getHeight() {
        return height;
    }

    public Diagram(){};

    public Diagram(ArrayList<Layer> Layer, double width, double height ){
        this.height=height;
        this.width=width;
        Layer.clear();

    }

    public void setSize( double w, double h ){
        this.width=w;
        this.height=h;
    }
    public void deleteCircle(){
        for( int i=0; i<listlayers.size(); i++ )
        {
            listlayers.get(i).deleteCircle();
        }
    }
    public void print(){
        for( int i=0; i< listlayers.size();i++)
        {
            listlayers.get(i).print();
        }
    }
    public void add(Layer a){
        listlayers.add(a);
    }

}
