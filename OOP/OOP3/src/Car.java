/**
 * @author ImKifu
 * @version 2.0
 * @since 10/12/2018
 */
public class Car {
    private String mode;
    private int day;
    private String name;

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
