/**
 * @author ImKifu
 * @version 2.0
 * @since 10/12/2018
 */
import static java.lang.Math.abs;

/**
 * Chương trình tìm ước số chung lớn nhất (USCLN)
 * của 2 số a và b
 */

// day la ham tim uoc chung lon nhat theo giai thuat Euclid
public class UCLN {
    public static int USCLN(int a, int b) {
        if (b == 0) return a;
        return USCLN(b, a % b);

    }

    public static int UCLN2(int a, int b)
    {
        a = abs(a); // trị tuyệt đối cho số âm
        b = abs(b);  // trị tuyệt đối cho số âm
        if (a == 0 && b != 0)
        {
            return b;
        }
        else if (a != 0 && b == 0)
        {
            return a;
        }
        while (a != b)
        {
            if (a > b)
            {
                a -= b;
            }
            else
            {
                b -= a;
            }
        }
        return a;

    }





}



