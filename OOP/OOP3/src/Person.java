/**
 * @author ImKifu
 * @version 2.0
 * @since 10/12/2018
 */
public class Person {
    private int weight;
    private int age;
    private String name;

    //ham constructor
    public Person(){

    }


    public int getWeight() {
        return weight;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }
}
